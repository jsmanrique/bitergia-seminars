## Blog posting recommendations

From the content format point of view, you should use the plain HTML. In any case, **avoid copy&paste from any rich editor or word processor to the blog rich editor**.

### HTML Recommendations

Following recommendations must be checked before publishing any blog post, so please, read them carefully and verify your post pass them.

#### Text content

Don't use any //inline styling// in your content (i.e. `<span font-weight="400">...</span>`)

Use [`<p>`](https://www.w3schools.com/Tags/tag_p.asp) tag to mark paragraphs.

If you want to mark something as important, use [`<strong>`](https://www.w3schools.com/Tags/tag_strong.asp) tag, and [`<em>`](https://www.w3schools.com/Tags/tag_em.asp) tag if it's less important, but relevant at least. I would avoid using [`<b>`](https://www.w3schools.com/Tags/tag_b.asp) and [`<i>`](https://www.w3schools.com/Tags/tag_i.asp) to emphasize anything.

For //pre-formatted text// (mainly code snippets) use :
* [`<code>`](https://www.w3schools.com/Tags/tag_code.asp) tag to mark the inline code 
```
<p>Don't forget to use the <code>alt</code> attribute in the images</p>
```

* and add [`<pre>`](https://www.w3schools.com/Tags/tag_pre.asp) tag for formatted block code. I.e.:
```lang=html
<pre><code>
def (name):
    print("Hello " + name)
</pre></code>
```

For special commands use [`<kbd>`](https://www.w3schools.com/Tags/tag_kbd.asp) tag:
```
<p>If you want to launch it, just type <kbd>mordred -c  mordred.cfg</kbd></p>
```

There are several kind of list possibilities:
* Unordered lists: Use [`<ul>`](https://www.w3schools.com/Tags/tag_ul.asp) tag to mark it, and [`<li>`](https://www.w3schools.com/Tags/tag_li.asp) tag to mark each item of the list
```
<ul>
  <li>First item</li>
  <li>Second item</li>
</ul>
```
* Ordered lists: Use [`<ol>`](https://www.w3schools.com/Tags/tag_ol.asp) tag to mark it, and [`<li>`](https://www.w3schools.com/Tags/tag_li.asp) tag to mark each item of the list
```
<ol>
  <li>First item</li>
  <li>Second item</li>
</ol>
```
* Definition lists: Use [`<dl>`](https://www.w3schools.com/Tags/tag_dl.asp) tag to mark it, and [`<dt>`](https://www.w3schools.com/Tags/tag_li.asp)  and [`<dd>`](https://www.w3schools.com/Tags/tag_dd.asp) tags to mark each term and its definition of the list
```
<dl>
  <dt>First item</dt>
   <dd>Definition of the first item</dd>
  <dt>Second item</dt>
   <dd>Definition of the second item</dd>
</dl>
```

If you write an abbreviation or an acronym, you must mark it with [`<abbr>`](https://www.w3schools.com/Tags/tag_abbr.asp) tag, using the `title` attribute to write the extended content. I.e.:
```lang=html
<p>You should read the <abbr title="World Wide Web Consortium">W3C</abbr> accessibility recommendations, known as <abbr title="Web Content Accessibility Guidelines">WCAG</abbr>.</p>
```

Use [`<blockquote>`](https://www.w3schools.com/Tags/tag_blockquote.asp) tag to mark a quote or cite, and add `cite` attribute if you know the link to the quote source. I.e.:
```lang=html
<blockquote cite="http://startupquotes.startupvitamins.com/post/101859447773/without-data-youre-just-another-person-with-an">Without data, you are just another person with an opinion</blockquote>
```

#### Tabular content

Never ever use [`<table>`](https://www.w3schools.com/Tags/tag_table.asp) tag to format non-tabular content.

If you need to format tabular content (mainly data), you must, at least, use:
* [`<thead>`](https://www.w3schools.com/Tags/tag_thead.asp) tag to mark table header rows ([`<tr>`](https://www.w3schools.com/Tags/tag_tr.asp)) and [`<th>`](https://www.w3schools.com/Tags/tag_th.asp) tag for each column header
* [`<tbody>`](https://www.w3schools.com/Tags/tag_tbody.asp) tag to mark table content rows ([`<tr>`](https://www.w3schools.com/Tags/tag_tr.asp)) and [`<td>`](https://www.w3schools.com/Tags/tag_td.asp) tag for each column data

For example:
|**IBM**|**Red Hat**
|100 commits|250 commits
|5 authors|3 authors
```lang=html
<table>
  <thead>
     <tr>
        <th>IBM</th>
        <th>Red Hat</th>
     </tr>
  </thead>
  <tbody>
     <tr>
        <td>100 commits</td>
        <td>250 commits</td>
     </tr>
     <tr>
        <td>5 authors</td>
        <td>3 authors</td>
     </tr>
  </tbody>
</table>
```

#### Images

They **must** have `alt` attribute. If the image is not decorative, the `alt` attribute should have a short description of the content of the image (i.e. `<img src="..." alt="Git commits evolution over time for Red Hat">`). Otherwise, it must be empty (`alt=""`).

### Links

If you want to link some content to a website, use [`<a>`](https://www.w3schools.com/Tags/tag_a.asp) tag. if it's an external site (not the blog, or your current site) , add `target="_blank"` attribute.

Link text must be meaningful. Don't use `click here` or similar texts.

#### Headers

To divide the blog post content in different sections, use [headers tags](https://www.w3schools.com/Tags/tag_hn.asp), starting by `<h2>` (Bitergia blog uses `<h1>` for the blog post title, so any section title would below that).

#### Youtube videos and other special content

For Youtube, Speakerdeck and Twitter content, just past the link to it (without any tag)

### Some questions you have to ask yourself before writing

#### Keywords

1. Try to investigate a little bit about the most popular sentences and words associated to the POST you are going to write: https://www.quicksprout.com/university/how-to-use-google-keyword-planning-tool/
2. Title: it should includes KEYWORDS, it must be attractive and must have sentences the people can use when a search in Google is done. 
3. Openings (//headers//): Google prefers bigger keywords.
4. Rest of the text: not too many keywords.
5. Context: Google  looks at the keywords as well as the synonyms. The rest of the words of the post must be connected with the keywords.
6. The first 100 words: Google emphasizes more in the words and sentences which are at the beginning of the page.

#### Internal links (linking to an internal site affects positively)

1. Cross links: linking to an internal post associated with yours becomes relevant. 
2. Old posts: associating older posts with the new ones once a month helps.
3. Anchor text: don’t fill the internal links with too many keywords. It must sounds natural.

#### External links (linking to an external sites helps)

1. Associated blogs: this helps to Google to identify which blogs are relevant for our blog.
2. Emails: notify to each blog author in order to let him know that you linked him, with this option other bloggers will share your posts.

#### Content

1. Duplicate content: if you take information from other source you have to link it and avoid having too much duplicate content.
2. Limited content: better the quality instead of quantity.
3. Length: have interrelationship between the length and the importance of the post. At least 2000 words.

#### URLs

1. Shorts:URLs shorts with keywords.  
2. Keywords: include them in the URL.
3. Numbers: the URL must have at least 3 consecutive numbers.

#### Pictures

1. Name of the file: descriptive with keywords.
2. `alt` label: you must include it.
3. Picture size: bigger size, better positioning.
4. File size: compressed but high quality.
5. Embedded code
6. Reality: better own pictures, better positioning.

#### Time to load

1. Media: videos, pictures or files optimized for the time of charge. https://developers.google.com/speed/
2. Javascript: try to minimize the number of external scripts you use.

# Useful resources

Keywords:
* [[https://keywordtool.io/ | Keyword Tool]] 
* [[http://adwords.google.com/keywordplanner | Keyword Planner]]
* [[http://tools.seobook.com/general/keyword-density/ | Keywords Density Analyzer de SEO-Book]]

Free images:
* [[http://www.freepik.es |  Freepik ]]
* [Pixabay](https://pixabay.com/)

Images editors:
* [[ https://www.canva.com/ | Canva ]]