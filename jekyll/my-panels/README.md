# First steps with Jekyll

Under this folder you have a set of _stages_ from 0 to 6 where you could test
how Jekyll works and produce a website.

## How to test each stage

Run `jekyll serve` in stage folder. For example:

```shell
$ cd stage0
$ jekyll serve
Configuration file: none
            Source: /home/jsmanrique/devel/bitergia/bitergia-seminars/jekyll/my-panels/stage0
       Destination: /home/jsmanrique/devel/bitergia/bitergia-seminars/jekyll/my-panels/stage0/_site
 Incremental build: disabled. Enable with --incremental
      Generating... 
                    done in 0.014 seconds.
 Auto-regeneration: enabled for '/home/jsmanrique/devel/bitergia/bitergia-seminars/jekyll/my-panels/stage0'
Configuration file: none
    Server address: http://127.0.0.1:4000/
  Server running... press ctrl-c to stop.

```

Check the output in your web browser in the noted url. Something like `http://127.0.0...:4000`