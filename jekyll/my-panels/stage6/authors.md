---
layout: default
---

# Authors

{% for author in site.data.authors %}
* [{{author.name}}](https//twitter.com/{{author.twitter}})
{% endfor %}