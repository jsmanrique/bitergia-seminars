# Jekyll

Transform your plain text into static websites and blogs.



# Running Jekyll

Super easy...

```shell
$ jekyll new my-site
New jekyll site installed in /home/..../my-panels.
$
```


```
my-panels
|-- about.md
|-- _config.yml
|-- css
|   `-- main.scss
|-- feed.xml
|-- _includes
|   |-- footer.html
|   |-- header.html
|   |-- head.html
|   |-- icon-github.html
|   |-- icon-github.svg
|   |-- icon-twitter.html
|   `-- icon-twitter.svg
|-- index.html
|-- js
|   |-- html5shiv.js
|   `-- respond.js
|-- _layouts
|   |-- default.html
|   |-- page.html
|   `-- post.html
|-- _posts
|   `-- 2018-09-27-welcome-to-jekyll.markdown
`-- _sass
    |-- _base.scss
    |-- _layout.scss
    |-- _normalize.scss
    `-- _syntax-highlighting.scss

6 directories, 22 files
```


To generate and test the website:

```shell
$ cd my-panels
$ jekyll serve
Configuration file: /home/..../my-panels/_config.yml
            Source: /home/..../my-panels
       Destination: /home/..../my-panels/_site
 Incremental build: disabled. Enable with --incremental
      Generating... 
                    done in 0.288 seconds.
 Auto-regeneration: enabled for '/home/..../my-panels'
Configuration file: /home/..../my-panels/_config.yml
    Server address: http://127.0.0.1:4000/
  Server running... press ctrl-c to stop.
```
Go to http://127.0.0.1:4000/



# Let's customize the site

1. Setup
2. Liquid
3. Front Matter
4. Layouts
5. Includes
6. Data Files
7. Assets
8. Blogging
9. Collections
10. Deployment


# Set up

To build the site in `_site` folder:
```shell
$ jekyll build
```

To build and test in `http://localhost:4000`:
```shell
$ jekyll serve --watch
```

Start by editing `_config.yml`. Every field listed there can be used in the templates as `{{site.<fieldname>}}`.


# Liquid

Template markup language developed by [Shopify](https://www.shopify.com/)

* Objects: `{{ }}`
* Tags: `{% %}`
* Filters: `{{ | <filter>}}`

More info at: 
* Liquid documentation: [shopify.github.io/liquid](https://shopify.github.io/liquid/)
* [Add-ons from Jekyll](https://jekyllrb.com/docs/liquid/) (filters and tags)


# Front matter

Meta information added to the documents in YAML format

```html
about.html
---
source: git
---

<p>This doc is about {{page.source}} panels</p>

```


# Layouts

Page layout defined in the front matter:

```html
---
layout: default
---

<h1>Hello World!</h1>
<p>A hello world to start, of course!</p>
```


Layouts are stored in `_layouts` folder.
```html
default.html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>{{site.title}}</title>
    </head>
    <body>
        {{ content }}
    </body>
</html>
```


# Includes

For pieces of code/content to be used several times.

They are stored in `_includes` folder.

```html
default.html
<!DOCTYPE html>
<html>
    <head>
        {% include head.html %}
    </head>
    <body>
        {% include header.html %}
        {% include sidebar.html %}
        {{ content }}
        {% include footer.html %}
    </body>
</html>
```


# Data files

Data as YAML, JSON or CSV files in `_data` folder.

```yml
people.yml
- name: Manrique
  twitter: jsmanrique
- name: Luis
  twitter: sanacl
```

```html
people.html
<ul>
    {% for person in site.data.people %}
    <li><a href="https://twitter.com/{{person.twitter}}">{{person.name}}</a></li>
    {% endfor %}
</ul>
```


# Assets

You can store assets (css, js, images, etc.) in regular folders.

You can use [Sass](https://sass-lang.com/)


`/assets/css/styles.scss`:
```
---
---
@import "main";
```

`_main.scss` placed in `_sass/`


# Blogging

Blog posts are placed in `_posts` folder.

Each one has the following format for the name:
`year-month-day-title.md` (for example: `2018-10-28-chaoss.md`)


Yes, it's a markdown file with its own front matter.
```
---
layout: post
---

These an example of CHAOSS panels
```


Some interesting fields that can be used:
* `page.date`: post date
* `page.title`: post title
* `site.posts`: posts *collection*
  * `post.url`: url to the post
  * `post.title`: post title
  * `post.excerpt`: 1st paragraph of the post


`_posts` is a _collection_. A set of contents that have a common structure, layout, etc.


# Collections

Content doesn't have to be grouped by date.

Let's say we would like to define a collection of _panels_, with a name, a description, and an screenshot.


1st, we define the collection in the `_config.yml` file:

```yml
collections:
  panels:
    output: true
```


Let's create our 1st panel document. It should be placed in `_panels` folder:
```
chaoss.md
---
short_name: chaoss
long_name: CHAOSS GMD
layout: panel
screenshot: chaoss.jpg
---

This is a panel for CHAOSS GMD metrics
```


Let's define its layout in `_layouts` folder:
```html
panel.html
---
layout: default
---

<h1>{{ page.long_name }}</h1>
<img src="./screenshots/{{ page.screenshot }}>
{{ content }}
```


Define collections layouts in `_config.yml`:
```yml
defaults:
  - scope:
      path: ""
      type: panels
    values:
      layout: panel
  - scope:
      path: ""
      type: posts
    values:
      layout: post
``` 


Let's create a page to list existing panels:
```html
panels.html
---
layout: default
---
<ul>
    {% for panel in site.panels %}
    <li><a href="{{ panel.url }}">{{ panel.long_name }}</a></li>
    {% endfor %}
</ul>
```


# Play with it

[Read more about it](https://jekyllrb.com)

Build your own website with Jekyll:
* [GitHub pages](https://pages.github.com/)
* [GitLab pages](https://about.gitlab.com/features/pages/)



# One more thing...

Bitergia website...