# Consuming data beyond Elasticsearch

Don't let Kibana limit your sight



# Data cosumption tools

* [Grafana](http://docs.grafana.org/features/datasources/elasticsearch/)
* [Google Colaboratory](https://colab.research.google.com/)
* [MyBinder](https://mybinder.org/)


# Grafana

![Grafana](img/grafana.svg)


## Get started

Documentation: [docs.grafana.org](http://docs.grafana.org)

```
$ docker run -d -p 3000:3000 grafana/grafana
```

Visit http://localhost:3000


## Connect with Elasticsearch

[Documentation](http://docs.grafana.org/features/datasources/elasticsearch/)

And let's have fun ... (*Demo time*)



# Google Colaboratory

https://colab.research.google.com/


## Connect with Elasticsearch

```
!pip install elasticsearch
!pip install elasticsearch-dsl

from elasticsearch import Elasticsearch, helpers
from elasticsearch_dsl import Search

ES_HOST = 'http://46.101.128.86:9200/'

es = Elasticsearch([ES_HOST])

s = Search(using=es, index='git')

response = s.execute()
```



# MyBinder

https://mybinder.org/


## Connect with Elasticsearch

1. Provide a `requirements.txt` file:
```
elasticsearch
elasticsearch-dsl
```

2. Build your notebook in your local environment
3. Push it in a repo
4. Share its *MyBinder URL*