#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from flask import Flask, abort
from flask_restful import Api, Resource, reqparse, fields, marshal

from elasticsearch import Elasticsearch, helpers

import uuid

ES_HOST = 'http://localhost:9200'

es = Elasticsearch(ES_HOST)

index_name = 'ecosystems'
es.indices.delete(index_name, ignore=[400, 404])
es.indices.create(index_name)

app = Flask(__name__)
api = Api(app)

meta_fields = {
    'key': fields.String,
    'value': fields.String
}

ecosystem_fields = {
    'name': fields.String,
    'git': fields.List(fields.String),
    'github': fields.List(fields.String),
    'meetup': fields.List(fields.String),
    'stackexchange': fields.List(fields.String),
    'meta': fields.List(fields.Nested(meta_fields)),
    'uri': fields.Url('ecosystem')
}

class EcosystemListAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('name', type = str, required = True, help = 'No name provided', location = 'json')
        self.reqparse.add_argument('git', type = str, required = False, help = 'No git repos provided', action = 'append')
        self.reqparse.add_argument('github', type = str, required = False, help = 'No GitHub repos provided', action = 'append')
        self.reqparse.add_argument('meetup', type = str, required = False, help = 'No Meetup groups provided', action = 'append')
        self.reqparse.add_argument('stackexchange', type = str, required = False, help = 'No StackOverflow tags provided', action = 'append')
        self.reqparse.add_argument('meta', type = dict, required = False, help = 'No metadata provided', action = 'append')
        super(EcosystemListAPI, self).__init__()

    def get(self):
        query = {"query": {"match_all" :{}}}
        ecosystems = []
        for item in helpers.scan(es, query, scroll = '300m', index = index_name):
            ecosystems.append(item['_source'])
        return {'ecosystems': [marshal(ecosystem, ecosystem_fields) for ecosystem in ecosystems]}

    def post(self):
        args = self.reqparse.parse_args()

        ecosystem = {}
        ecosystem['id'] = uuid.uuid4().hex

        for k, v in args.items():
            if v is not None:
                ecosystem[k] = v
        
        es.index(index = index_name, doc_type = 'ecosystem', id = ecosystem['id'], body = ecosystem)
        
        return {'ecosystem': marshal(ecosystem, ecosystem_fields)}, 201

class EcosystemAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('name', type = str, required = True, help = 'No name provided', location = 'json')
        self.reqparse.add_argument('git', type = str, required = False, help = 'No git repos provided', action = 'append')
        self.reqparse.add_argument('github', type = str, required = False, help = 'No GitHub repos provided', action = 'append')
        self.reqparse.add_argument('meetup', type = str, required = False, help = 'No Meetup groups provided', action = 'append')
        self.reqparse.add_argument('stackexchange', type = str, required = False, help = 'No StackOverflow tags provided', action = 'append')
        super(EcosystemAPI, self).__init__()

    def get (self, id):
        try:
            ecosystem_data = es.get(index = index_name, doc_type = 'ecosystem', id = id)
            ecosystem = ecosystem_data['_source']
        except Exception:
            abort(404)
        return {'ecosystem': marshal(ecosystem, ecosystem_fields)}
    
    def put(self, id):
        try:
            ecosystem_data = es.get(index = index_name, doc_type = 'ecosystem', id = id)
            ecosystem = ecosystem_data['_source']
        except Exception:
            abort(404)
        
        args = self.reqparse.parse_args()
        for k, v in args.items():
            if v is not None:
                ecosystem[k] = v
        
        es.index(index = index_name, doc_type = 'ecosystem', id = ecosystem['id'], body = ecosystem)

        return{'ecosystem': marshal(ecosystem, ecosystem_fields)}

    def delete(self, id):
        try:
            es.delete(index = index_name, doc_type = 'ecosystem', id = id)
        except Exception:
            abort(404)
        
        return{'result': True}

api.add_resource(EcosystemListAPI, '/projects/api/v1.0/ecosystems', endpoint = 'ecosystems')
api.add_resource(EcosystemAPI, '/projects/api/v1.0/ecosystems/<string:id>', endpoint = 'ecosystem')

if __name__ == '__main__':
    app.run(port=5000, debug=True)