# REST APis with Flask-RESTful



# REST


* Client-Server
* Stateless
* Cacheable
* Layered System
* Uniform Interface
* Code on demand


# REST verbs

|HTTP Method|Action|Example|
|-----------|------|-------|
|GET|Obtain information about a resource|http://example.com/api/orders http://example.com/api/orders/123|


|HTTP Method|Action|Example|
|-----------|------|-------|
|POST|Create a new resource|http://example.com/api/orders (data provided in the request)|


|HTTP Method|Action|Example|
|-----------|------|-------|
|PUT|Update a resource|http://example.com/api/orders/123 (data provided in the request)|


|HTTP Method|Action|Example|
|-----------|------|-------|
|DELETE|Delete a resource|http://example.com/api/orders/123|


# Further _reading_

* [Wikipedia](https://en.wikipedia.org/wiki/Representational_state_transfer)
* [Microservices, the big picture (Pluralsight)](https://www.pluralsight.com/courses/microservices-big-picture)



# Flask

[flask.pocoo.org](http://flask.pocoo.org/)

![flask](./imgs/flask-white.png)

[Pirates use Flask, the Navy uses Django](https://medium.com/@alanhamlett/pirates-use-flask-the-navy-uses-django-a0225c80a46e)


## Flask is fun

Install
```
(flask)$ pip install flask
```
Code
```
from flask import Flask
app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!"

if __name__ == '__main__':
    app.run(debug=True)
```
Run
```
(flask)$ python3 app.py
```
Visit `http://localhost:5000`


## Flask out-of-the-box features

* built-in development server and debugger
* integrated unit testing support
* RESTful request dispatching
* uses Jinja2 templating
* support for secure cookies (client side sessions)
* 100% WSGI 1.0 compliant
* Unicode based
* extensively documented 



# Flask-RESTful

[flask-restful.readthedocs.io/](https://flask-restful.readthedocs.io/en/latest/)

![flask-restful](./imgs/flask-restful-white.png)


## Get started

Inspiration: [blog.miguelgrinberg.com/post/designing-a-restful-api-using-flask-restful](https://blog.miguelgrinberg.com/post/designing-a-restful-api-using-flask-restful) (yes, 5yr old post!)

```
(flask)$ pip install flask-restful
```


## Code

```
from flask import Flask
from flask_restful import Api, Resource

app = Flask(__name__)
api = Api(app)

class TaskListAPI(Resource):
    def get(self):
        pass

    def post(self):
        pass

class TaskAPI(Resource):
    def get(self, id):
        pass

    def put(self, id):
        pass

    def delete(self, id):
        pass

api.add_resource(TaskListAPI, '/todo/api/v1.0/tasks', endpoint = 'tasks')
api.add_resource(TaskAPI, '/todo/api/v1.0/tasks/<int:id>', endpoint = 'task')

if __name__ == '__main__':
    app.run(debug=True)
```


## Request Parsing and Validation

```
from flask_restful import Api, Resource
# ...
class TaskListAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('title', type = str, required = True,
            help = 'No task title provided', location = 'json')
        self.reqparse.add_argument('description', type = str, default = "", location = 'json')
        super(TaskListAPI, self).__init__()
    #...
    def post(self):
        args = self.reqparse.parse_args()
        # ...

class TaskAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('title', type = str, location = 'json')
        self.reqparse.add_argument('description', type = str, location = 'json')
        self.reqparse.add_argument('done', type = bool, location = 'json')
        super(TaskAPI, self).__init__()

    # ...
```


## Generating Responses

```
from flask_restful import fields, marshal

task_fields = {
    'title': fields.String,
    'description': fields.String,
    'done': fields.Boolean,
    'uri': fields.Url('task')
}

class TaskAPI(Resource):
    # ...

    def put(self, id):
        # ...
        return { 'task': marshal(task, task_fields) }
```



# Let's see an example
![babyborn toys](https://cdn.pixabay.com/photo/2016/05/30/23/24/ducks-1426008_1280.jpg)



# Further reading

* [Hypermedia APIs](https://www.programmableweb.com/news/hypermedia-apis-benefits-hateoas/how-to/2014/02/27)
* [Flask-JSONPrc](https://github.com/cenobites/flask-jsonrpc)


```
from flask import Flask
from flask_jsonrpc import JSONRPC

app = Flask(__name__)
jsonrpc = JSONRPC(app, '/api')

@jsonrpc.method('App.index')
def index():
    return u'Welcome to Flask JSON-RPC'
```


```
$ curl -i -X POST \
   -H "Content-Type: application/json; indent=4" \
   -d '{
    "jsonrpc": "2.0",
    "method": "App.index",
    "params": {},
    "id": "1"
}' http://localhost:5000/api
HTTP/1.0 200 OK
Content-Type: application/json
Content-Length: 77
Server: Werkzeug/0.8.3 Python/2.7.3
Date: Fri, 14 Dec 2012 19:26:56 GMT

{
  "jsonrpc": "2.0",
  "id": "1",
  "result": "Welcome to Flask JSON-RPC"
}
```