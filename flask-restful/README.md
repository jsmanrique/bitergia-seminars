# Flask-RESTFUL seminar

You can read the [slides](slides.md), or view them using [Reveal.js](https://github.com/hakimel/reveal.js/). 
(check [my custom theme](https://gitlab.com/jsmanrique/bitergia-revealjs-theme) 
if you want to have the same look & feel I've used during the seminar)

## To run the demo

I recommend to use a python virtual environment. For example:

```
$ python3 -m venv flask
$ source flask/bin/activate
(flask) $
```

The demo expects an elasticsearch running in `http://localhost:9200`. You can change
that by changing the `ES_HOST` variable in the [api.py](demo/api.py) file.

Install required packages:
```
(flask) $ pip install flask-restful
...
(flask) $ pip install elasticsearch
```

Now you can run it with:
```
(flask) $ python3 demo/api.py
```

The api.py file is short, so you should be able to read the available methods,
expected inputs and outputs.